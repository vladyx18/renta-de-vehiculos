// UNIVERSIDAD MARIANO GALVEZ
// PROGRAMA PARA EL ALQUILER DE AUTOMOVILES
// MIEMBROS: VLADIMIR MUNRAYOS
// CARN�:    1190-14-1874


#include "stdafx.h"
#include <math.h>
#include <conio.h>
#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>

using namespace std;


struct alquiler{
	char nombre[50];
	char modelo[50];
	int tiempo;
	char tarjeta[20];
	int cantidad;
};


bool leer();

int main(int argc, char *argv[])
{
	system("color 03");
	cout << "-------------------------------------------------\n";
	cout << "BIENVENIDO AL PROGRAMA PARA ALQUILER DE AUTOMOVIL\n";
	cout << "-------------------------------------------------\n";
	cout << "MENU\n";
	int opr;
    menu:
	cout << "1. VER LISTA DE CARROS A ALQUILAR\n";
	cout << "2. ALQUILAR UN AUTOMOVIL\n";
	cout << "3. MOSTRAR EL ALMACENAMIENTO DE LOS ALQUILERES\n";
	cout << "4. SALIR\n";
	scanf_s("%d",&opr);
	system("cls");
	
	if (opr==1){
		string readfromfile;
		ifstream r_file("inventario.txt");

		if (r_file.is_open())
		{
			while (r_file.good()){
				getline(r_file, readfromfile);
				cout << readfromfile << endl << endl;
			}

		}
		else
			cout << "El archivo no existe!" << endl;
		_getch();
		system("cls");
		goto menu;
	}
	
	if (opr==2){
		FILE *file;
		alquiler nuevo;
		cout << "Ingrese nombre completo de la persona\t";
		cin.ignore();
		cin.getline(nuevo.nombre,50);
		cout << "Ingrese modelo de carro a alquilar\t";
		cin.getline(nuevo.modelo,50);
		cout << "Ingrese tiempo del alquiler por dias\t";
		cin >> nuevo.tiempo;
		cout << "Cantidad a pagar por el alquiler\t";
		cin >> nuevo.cantidad;
		cout << "El total a pagar por el alquiler es: " << nuevo.tiempo * nuevo.cantidad << endl;
		cout << "Ingrese no. de tarjeta de credito para el pago\t";
		cin.ignore();
		cin.getline(nuevo.tarjeta,20);
		file = fopen("almacenamiento.txt", "ab");
		if (file==NULL)
		{
			cout << "Error al abrir el archivo!";
			exit(1);
		}
		fwrite(&nuevo, sizeof(struct alquiler), 1, file);
		fclose(file);
		system("cls");
		goto menu;
		_getch();
	}

	if (opr == 3){
		leer();
		_getch();
		system("cls");
		goto menu;
	}
}


bool leer(){
	FILE*reg;
	alquiler nuevo;
	int cantreg;
	reg = fopen("almacenamiento.txt", "rb");
	if (reg == NULL)
	{
		cout << "Error al abrir el archivo!";
		exit(1);
	}
	fseek(reg, 0, 2);
	cantreg = (ftell(reg)) / sizeof(struct alquiler);
	for (int i = 0; i < cantreg; i++){
		fseek(reg, i*sizeof(struct alquiler), 0);
		fread(&nuevo, sizeof(struct alquiler), 1, reg);
		cout << "Nombre: " << nuevo.nombre << endl;
		cout << "modelo del carro: " << nuevo.modelo << endl;
		cout << "tiempo: " << nuevo.tiempo << endl;
		cout << "cantidad a pagar: " << nuevo.cantidad << endl;
		cout << "total a pagar: " << nuevo.tiempo * nuevo.cantidad << endl;
		cout << "No. de tarjeta: " << nuevo.tarjeta << endl;
		cout << endl;
	}
	fclose(reg);
	return(true);
}